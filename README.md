# Web Security Report, December '16, Higher Diploma in Computer Science #

This document is a report on web application security. The application used for demonstration in the
report is a microblogging/ twitter-clone application built on node.js and called MyTweet. 

The report covers threat modelling, authentication &encryption, and the filtering
& sanitisation of input and output.